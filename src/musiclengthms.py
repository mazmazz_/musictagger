# coding: utf-8
# %load nightsresize
import struct
import argparse
import os
from gooey import Gooey, GooeyParser
from omg import *
import mutagen
import shutil
import math
import tempfile
import itertools

##### setup #####

@Gooey(
    program_name = 'Music LengthMS Tagger',
    required_cols = 1,
    optional_cols = 1,
)
def main():
    args = get_args()
    ##### user vars #####

    in_filenames = args.infile.split(';')
    loopms_values = args.loopms or []
    loopendms_values = [] #args.loopendms
    do_backup = args.backup
    log_verbose = args.verbose

    ##### magic happens here #####

    for in_filename, loopms_value, loopendms_value in itertools.zip_longest(in_filenames, loopms_values, loopendms_values):
        if not os.path.isfile(in_filename):
            print('Path %s not found or is not a file' % in_filename)
            continue

        def copy_backup(fn):
            if do_backup:
                fn_pieces = os.path.splitext(os.path.basename(fn))
                backup_fn = os.path.join(os.path.dirname(fn), '{}{}.backup'.format(fn_pieces[0],
                    fn_pieces[-1] if len(fn_pieces) > 1 else ''))
                shutil.copyfile(fn, backup_fn)
                if log_verbose:
                    print('Copied {}'.format(backup_fn))

        #################################
        # File Reading

        print('Reading %s' % in_filename)

        # Is this an audio file?
        try:
            result = mutagen.File(in_filename)
            copy_backup(in_filename)
            lengthms, loopms, loopendms = do_audiofile(result, in_filename,
                loopms=loopms_value,
                loopendms=loopendms_value,
                remove_lengthms=args.remove,
                log_verbose=log_verbose)
            print('Saved audio file {}{}{}{}'.format(os.path.basename(in_filename),
                ', LENGTHMS={}'.format(lengthms) if lengthms is not None else '',
                ', LOOPMS={}'.format(loopms) if loopms is not None else '',
                ', LOOPENDMS={}'.format(loopendms) if loopendms is not None else '')
            )
            return
        except (mutagen.MutagenError, TypeError) as e:
            if log_verbose:
                print('{} is not an audio file, trying as WAD: {}'.format(os.path.basename(in_filename), e))

        # Is this a WAD?
        try:
            result = omg.WadIO()
            result.open(in_filename)
            copy_backup(in_filename)
            do_wadfile(result, in_filename,
                lumpnames=args.lumpnames,
                rewrite=args.rewrite,
                loopms_values=loopms_values,
                loopendms_values=loopendms_values,
                remove_lengthms=args.remove,
                log_verbose=log_verbose
            )
        except (TypeError, IOError) as e:
            print('Error loading WAD file {}: {}'.format(os.path.basename(in_filename), e))

#################################
# WAD File
#################################

def do_wadfile(result, in_filename, lumpnames=None, rewrite=False, loopms_values=None, loopendms_values=None, remove_lengthms=False, log_verbose=False):
    if not isinstance(result, omg.WadIO):
        raise TypeError('Input data is not a WAD!')

    lumpnames = lumpnames or []
    loopms_values = loopms_values or []
    loopendms_values = loopendms_values or []
    lumpnum = 0
    matchinglumpnum = 0
    musiclumpnum = 0

    for i, entry in enumerate(result.entries):
        lumpnum += 1
        if len(lumpnames) == 0 or entry.name in lumpnames:
            if entry.name in lumpnames:
                loopms_value = lumpnames.loopms_values[lumpnames.index(entry.name)] if len(loopms_values) > lumpnames.index(entry.name) else None
                loopendms_value = lumpnames.loopendms_values[lumpnames.index(entry.name)] if len(loopendms_values) > lumpnames.index(entry.name) else None
            else:
                loopms_value, loopendms_value = None, None
            matchinglumpnum += 1
            tf = None
            try:
                lump = result.read(i)

                # Handle tempfile existence ourselves, because Windows doesn't do this right
                tf = tempfile.NamedTemporaryFile(delete=False)
                tf.write(lump)
                tf.flush()
                tf.close()

                # now operate!
                lengthms, loopms, loopendms = try_audiodata(tf.name, entry.name, in_filename,
                    loopms=loopms_value,
                    loopendms=loopendms_value,
                    remove_lengthms=remove_lengthms,
                    log_verbose=log_verbose)

                # read our temp file back
                with open(tf.name, 'rb') as tff:
                    lump = tff.read()

                result.update(i, lump)

                print('Updated {}{}{}{}'.format(entry.name,
                    ', LENGTHMS={}'.format(lengthms) if lengthms is not None else '',
                    ', LOOPMS={}'.format(loopms) if loopms is not None else '',
                    ', LOOPENDMS={}'.format(loopendms) if loopendms is not None else '')
                )
                musiclumpnum += 1
            except (TypeError, mutagen.MutagenError) as e:
                if log_verbose:
                    print('Lump {} {}: {}'.format(entry.name,
                        'is not an audio file' if isinstance(e, TypeError) else 'processing error',
                        e)
                    )
            finally:
                if isinstance(tf, tempfile._TemporaryFileWrapper):
                    os.remove(tf.name)
        elif log_verbose:
            print('Lump {} does not match lumpname list.'.format(lumpname))

    if rewrite:
        result.rewrite()
    else:
        result.save()

    print('Processed {} lumps, {}{} music lumps'.format(lumpnum,
        '{} matching lumps, '.format(matchinglumpnum) if len(lumpnames) > 0 else '',
        musiclumpnum)
    )

    print('Saved {}\n'.format(in_filename))

#################################
# Audio File
#################################

def try_audiodata(data, lumpname, in_filename, loopms=None, loopendms=None, remove_lengthms=False, log_verbose=False):
    audio = mutagen.File(data)
    return do_audiofile(audio, in_filename,
        loopms=loopms,
        loopendms=loopendms,
        remove_lengthms=remove_lengthms,
        log_verbose=log_verbose)

def do_audiofile(result, in_filename, loopms=None, loopendms=None, remove_lengthms=False, log_verbose=False):
    if not isinstance(result, mutagen.FileType):
        raise TypeError('Input audio data is not a Mutagen FileType!')

    length = None
    if remove_lengthms:
        if 'LENGTHMS' in result.tags:
            del result.tags['LENGTHMS']
    else:
        length = str(math.floor(result.info.length * 1000))
        result.tags['LENGTHMS'] = length

    # convert LOOPPOINT= to LOOPMS
    # remove both LOOPPOINT= tag and COMMENT=LOOPPOINT= tag, but latter one gets priority
    # from mixer_sound: ((44.1L+atoi(p)) / 44100.0L) * 1000 for milliseconds
    try:
        if 'LOOPPOINT' in result.tags:
            looppoint = int(result.tags['LOOPPOINT'][0])
            result.tags['LOOPMS'] = str(math.floor(((result.info.sample_rate/1000) + looppoint) / result.info.sample_rate * 1000))
            del result.tags['LOOPPOINT']

        if 'COMMENT' in result.tags and 'LOOPPOINT=' in result.tags['COMMENT'][0]:
            looppoint = int(result.tags['COMMENT'][0].split('=')[-1])
            result.tags['LOOPMS'] = str(math.floor(((result.info.sample_rate/1000) + looppoint) / result.info.sample_rate * 1000))
            del result.tags['COMMENT']
    except e:
        print('Tried to convert LOOPPOINT=, error: {}'.format(e))

    # now insert LOOPMS= and LOOPENDMS=
    for name, value in zip(['LOOPMS', 'LOOPENDMS'], [loopms, loopendms]):
        if isinstance(value, (int,float,complex)):
            value = str(math.floor(value))
        if isinstance(value, str) and value.isdigit():
            result.tags[name] = value

    result.save()

    return (length,
        result.tags['LOOPMS'][0] if 'LOOPMS' in result.tags else None,
        result.tags['LOOPENDMS'][0] if 'LOOPENDMS' in result.tags else None
    )

#################################
# Arguments
#################################

def get_args():
    parser = GooeyParser(description='Tag music lumps with LENGTHMS= automatically.')
    parser.add_argument('infile', type=str, help='Input music file(s) or WAD file(s), no PK3s!', widget='MultiFileChooser')
    parser.add_argument('-lumpnames', '-l', type=str, nargs='+', help='Specific music names to operate, space-separated: "O_GFZ1 O_GFZ2 O_BOSS"', default=None)
    parser.add_argument('-loopms', '-lm', type=int, nargs='+', help='Add LOOPMS= tag with this value, space-separated if you specified multiple lumps: "10000 20000 30000"', default=None)
    #parser.add_argument('-loopendms', '-lem', type=int, nargs='+', help='Add LOOPENDMS= tag with this value, space-separated if you specified multiple lumps: "10000 20000 30000"', default=None)
    parser.add_argument('-remove', '-r', help='Remove LENGTHMS= tag if it exists.', action='store_true', default=False)
    parser.add_argument('-backup', '-b', help='Backup file before operating: "[filename].backup"', action='store_true', default=False)
    parser.add_argument('-rewrite', '-rw', help='Rewrite entire WAD file to get rid of waste data.', action='store_true', default=False)
    parser.add_argument('-verbose', '-v', help='Print more logs.', action='store_true', default=False)
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()

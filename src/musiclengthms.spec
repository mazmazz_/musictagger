# -*- mode: python -*-

block_cipher = None


a = Analysis(['musiclengthms.py'],
             pathex=['D:\\Programs\\WAD\\Scripts'],
             binaries=[],
             datas=[('d:\\projects\\gooey\\gooey\\images', 'gooey\\images'), ('d:\\projects\\gooey\\gooey\\languages', 'gooey\\languages')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='musiclengthms',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False )
